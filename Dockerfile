FROM golang:buster

RUN mkdir /restapi

ADD . /go/src/gitlab.com/vorashilf/restapi

WORKDIR /go/src/gitlab.com/vorashilf/restapi

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["go", "run", "main/main.go"]