package structs

import (
	"database/sql"
)

type Post struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Subtext     string `json:"subtext"`
	MainContent string `json:"maincontent"`
}

func (p *Post) GetPost(db *sql.DB) error {
	return db.QueryRow("SELECT title,subtext, main_content from blog.posts where id = $1", p.ID).Scan(&p.Title, &p.Subtext ,&p.MainContent)
}

func (p *Post) UpdatePost(db *sql.DB) error {
	_, err := db.Exec("UPDATE blog.posts SET title=$1, subtext=$2, main_content=$3 where id=$4 ", p.Title, p.Subtext, p.MainContent, p.ID)
	return err
}

func (p *Post) DeletePost(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM blog.posts WHERE id=$1 ", p.ID)
	return err
}

func (p *Post) CreatePost(db *sql.DB) error {
	err := db.QueryRow(
		"INSERT INTO blog.posts(title, subtext, main_content) VALUES($1, $2, $3) RETURNING id ",
		p.Title, p.Subtext, p.MainContent).Scan(&p.ID)

	if err != nil {
		return err
	}

	return nil
}

func GetPosts(db *sql.DB, start, count int) ([]Post, error) {
	rows, err := db.Query("SELECT id, title, subtext,main_content from blog.posts  order by id desc limit $1 offset $2 ", count, start)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	posts := []Post{}
	for rows.Next() {
		var post Post
		if err := rows.Scan(&post.ID, &post.Title, &post.Subtext, &post.MainContent); err != nil {
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, nil
}


