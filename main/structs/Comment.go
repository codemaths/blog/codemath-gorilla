package structs

import (
	"database/sql"
	"time"
)

type Comment struct {
	ID        string    `json:"id"`
	PostId    int       `json:"post_id"`
	Author    string    `json:"author"`
	Email     string    `json:"email"`
	Content   string    `json:"content"`
	DateAdded time.Time `json:"date_added"`
}

func GetComments(db *sql.DB, postId int) ([]Comment, error) {
	rows, err := db.Query("SELECT id, post_id, author, content, date_added from blog.comments where post_id = $1", postId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var comments []Comment

	for rows.Next() {
		var comment Comment
		if err := rows.Scan(&comment.ID, &comment.PostId, &comment.Author, &comment.Content, &comment.DateAdded); err != nil {
			return nil, err
		}
		comments = append(comments, comment)
	}
	return comments, nil
}

func (comment *Comment) AddComment(db *sql.DB) error {
	err := db.QueryRow(
		"INSERT INTO blog.comments(post_id, author, email, content) VALUES($1, $2, $3, $4) RETURNING id ",
		comment.PostId,
		comment.Author,
		comment.Email,
		comment.Content).Scan(&comment.ID)

	if err != nil {
		return err
	}

	return nil
}
