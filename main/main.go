package main

import (
	"fmt"
	"github.com/subosito/gotenv"
	. "gitlab.com/vorashilf/restapi/main/app"
	"os"
)

func init() {
	gotenv.Load("./security/env")
}


func main() {
	a := App{}
	a.Initialize(os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DB"), os.Getenv("POSTGRES_ADDRESS"))
	fmt.Println("Starting Service...")
	a.Run(":8000")
	fmt.Println("Service has finished.")

}
