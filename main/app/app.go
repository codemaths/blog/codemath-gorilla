package app

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gitlab.com/vorashilf/restapi/main/structs"
	"gitlab.com/vorashilf/restapi/main/util"
	"log"
	"net/http"
	"os"
	"strconv"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

func (a *App) Initialize(user, password, dbname, dbaddress string) {
	connectionString :=
		fmt.Sprintf("postgres://%s:%s@%s:5432/%s?sslmode=disable", user, password, dbaddress, dbname)
	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		fmt.Println(err)
		log.Print(err)
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	a.initializePostRoutes()
	a.initializeCommentsRoutes()
}

func (a *App) initializePostRoutes() {
	a.Router.Handle("/api/posts", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.getPosts))).Methods("GET")
	a.Router.Handle("/api/posts", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.createPost))).Methods("POST")
	a.Router.Handle("/api/post/{post_id:[0-9]+}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.getPost))).Methods("GET")
	a.Router.Handle("/api/post/{post_id:[0-9]+}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.updatePost))).Methods("PUT")
	a.Router.Handle("/api/post/{post_id:[0-9]+}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.deletePost))).Methods("DELETE")
}

func (a *App) initializeCommentsRoutes() {
	a.Router.Handle("/api/post/{post_id:[0-9]+}/comments", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.getCommentsForPost))).Methods("GET")
	a.Router.Handle("/api/post/{post_id:[0-9]+}/comments", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(a.addCommentForPost))).Methods("POST")
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (a *App) getPost(writer http.ResponseWriter, request *http.Request) {
	routeVariables := mux.Vars(request)
	post_id, err := strconv.Atoi(routeVariables["post_id"])

	if err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid post ID")
	}

	post := structs.Post{ID: post_id}
	if err := post.GetPost(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			util.RespondWithJson(writer, http.StatusNoContent, post)
		default:
			util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		}
		return
	}
	util.RespondWithJson(writer, http.StatusOK, post)
}

func (a *App) getPosts(writer http.ResponseWriter, request *http.Request) {
	count, _ := strconv.Atoi(request.FormValue("count"))
	start, _ := strconv.Atoi(request.FormValue("start"))

	if count > 10 || count < 1 {
		count = 10
	}

	if start < 0 {
		start = 0
	}

	posts, err := structs.GetPosts(a.DB, start, count)

	if err != nil {
		fmt.Println(err)
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}
	if len(posts) == 0 {
		util.RespondWithJson(writer, http.StatusNoContent, posts)
		return
	}
	util.RespondWithJson(writer, http.StatusOK, posts)
}

func (a *App) createPost(writer http.ResponseWriter, request *http.Request) {
	var post structs.Post
	decoder := json.NewDecoder(request.Body)
	if err := decoder.Decode(&post); err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid request payload.")
		return
	}

	defer request.Body.Close()

	if err := post.CreatePost(a.DB); err != nil {
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}

	util.RespondWithJson(writer, http.StatusCreated, post)
}

func (a *App) updatePost(writer http.ResponseWriter, request *http.Request) {
	routeVariables := mux.Vars(request)
	post_id, err := strconv.Atoi(routeVariables["post_id"])
	if err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid post ID")
		return
	}

	var fetchedPost structs.Post
	decoder := json.NewDecoder(request.Body)
	if err := decoder.Decode(&fetchedPost); err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer request.Body.Close()
	fetchedPost.ID = post_id

	if err := fetchedPost.UpdatePost(a.DB); err != nil {
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}

	util.RespondWithJson(writer, http.StatusOK, fetchedPost)
}

func (a *App) deletePost(writer http.ResponseWriter, request *http.Request) {
	routeVariables := mux.Vars(request)
	post_id, err := strconv.Atoi(routeVariables["post_id"])
	if err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid Post ID")
		return
	}

	post := structs.Post{ID: post_id}
	if err := post.DeletePost(a.DB); err != nil {
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}

	util.RespondWithJson(writer, http.StatusOK, map[string]string{"result": "success"})
}

func (a *App) getCommentsForPost(writer http.ResponseWriter, request *http.Request) {
	routeVariables := mux.Vars(request)
	postId, err := strconv.Atoi(routeVariables["post_id"])

	if err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid post id")
	}

	// Validate Post Exist
	post := structs.Post{ID:postId}
	if err := post.GetPost(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			util.RespondWithError(writer, http.StatusBadRequest, "Post not found")
		default:
			util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		}
		return
	}

	comments, err := structs.GetComments(a.DB, postId)

	if err != nil {
		fmt.Println(err)
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}

	if len(comments) == 0 {
		util.RespondWithJson(writer, http.StatusNoContent, comments)
		return
	}
	util.RespondWithJson(writer, http.StatusOK, comments)

}

func (a *App) addCommentForPost(writer http.ResponseWriter, request *http.Request) {
	routeVariables := mux.Vars(request)
	postId, err := strconv.Atoi(routeVariables["post_id"])

	if err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid post id")
	}
	var comment structs.Comment
	decoder := json.NewDecoder(request.Body)

	if err := decoder.Decode(&comment); err != nil {
		util.RespondWithError(writer, http.StatusBadRequest, "Invalid request payload.")
		return
	}

	defer request.Body.Close()
	comment.PostId = postId
	if err := comment.AddComment(a.DB); err != nil {
		util.RespondWithError(writer, http.StatusInternalServerError, err.Error())
		return
	}

	util.RespondWithJson(writer, http.StatusCreated, comment)

}
