package main_test

import (
	"testing"
)

func TestNoComments(t *testing.T) {
	givenThereIsOnePostInDatabaseWithoutComments()
	whenIMakeARequestToGetCommentsOfThatPost()
	thenIGet204AndEmptyListOfComments(t)
}

func TestGetCommentsOfNonExistingPost(t *testing.T) {
	givenThereIsOnePostInDatabaseWithoutComments()
	whenIMakeARequestToGetCommentsOfNonExistingPost()
	thenIGet400WithErrorMessage(t)
}

func TestGetComments(t *testing.T) {
	givenThereIsOnePostInDatabaseWithComments()
	whenIMakeARequestToGetCommentsOfThatPost()
	thenIGet200AndNonEmptyListOfComments(t)
}

func TestGetCommentsOfDifferentPosts(t *testing.T) {
	givenThereAreTwoPostsInDatabaseWithComments()
	whenIMakeARequestToGetCommentsOfFirstPost()
	thenIGetNComments(t, 4)
	whenIMakeARequestToGetCommentsOfSecondPost()
	thenIGetNComments(t, 3)
}
