package main_test

import (
	"bytes"
	"encoding/json"
	"github.com/joho/godotenv"
	. "gitlab.com/vorashilf/restapi/main/app"
	"gitlab.com/vorashilf/restapi/main/structs"
	"gitlab.com/vorashilf/restapi/test/helper"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a App
var response *httptest.ResponseRecorder

//init is invoked before main()
func init() {
	// loads values from env into the system
	if err := godotenv.Load("./.env-test"); err != nil {
		log.Print("No env file found")
	}
}

func TestMain(m *testing.M) {
	a = App{}
	user, password, db, address := os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_DB"), os.Getenv("POSTGRES_ADDRESS")
	a.Initialize(user, password, db, address)
	helper.EnsureTablesExists(&a)
	code := m.Run()
	helper.ClearAllTables(&a)
	os.Exit(code)
}


func givenAllTablesAreClear() {
	helper.ClearAllTables(&a)
}

func whenIMakeARequestToGetAllPosts() {
	request, _ := http.NewRequest("GET", "/api/posts", nil)
	response = helper.ExecuteRequest(request, &a)
}

func thenIGet204AndEmptyListOfPosts(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusNoContent, response.Code)

	if body := response.Body.String(); body != "[]" {
		t.Errorf("Expected an empty array. Got %s.", body)
	}
}

func whenIMakeARequestToGetAPostThatDoesNotExist() {
	request, _ := http.NewRequest("GET", "/api/post/11", nil)
	response = helper.ExecuteRequest(request, &a)
}

func thenIGet204WithErrorMessage(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusNoContent, response.Code)
}

func whenIMakeAPOSTRequestToCreateANewPost() {
	var testJson = "{\"title\":\"test title\", \"subtext\":\"test subtext\", \"maincontent\" : \"test maincontent\"}"
	payload := []byte(testJson)

	request, _ := http.NewRequest("POST", "/api/posts", bytes.NewBuffer(payload))
	response = helper.ExecuteRequest(request, &a)
}

func thenICreateANewPostAndAllFieldsAreCorrectlyAdded(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusCreated, response.Code)

	var actualResponseMap map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &actualResponseMap)

	if actualResponseMap["title"] != "test title" {
		t.Errorf("Expected post title to be 'test title'. Got '%v'", actualResponseMap["title"])
	}

	if actualResponseMap["subtext"] != "test subtext" {
		t.Errorf("Expected post subtext to be 'test subtext'. Got '%v'", actualResponseMap["subtext"])
	}

	if actualResponseMap["maincontent"] != "test maincontent" {
		t.Errorf("Expected post maincontent to be 'test maincontent. Got '%v'", actualResponseMap["maincontent"])
	}

	// The id is compared to 1.0 because JSON unmarshalling converts numbers to
	// floats, when the target is a map[string]interface{}
	if actualResponseMap["id"] != 1.0 {
		t.Errorf("Expected post id to be '1'. Got '%v'", actualResponseMap["id"])
	}
}

func givenThereIsOnePostInDatabase() {
	givenAllTablesAreClear()
	helper.AddPosts(1, &a)
}

func whenIMakeARequestToGetThatPost() {
	request, _ := http.NewRequest("GET", "/api/post/1", nil)
	response = helper.ExecuteRequest(request, &a)
}

func theIGet200AndThatPost(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusOK, response.Code)
}
func whenIMakeARequestToGetCopyOfThatPost() map[string]interface{}{
	whenIMakeARequestToGetThatPost()

	var originalPost map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &originalPost)
	return originalPost
}

func andUpdateThatOnePost() map[string]interface{} {
	var testJson = "{\"title\":\"test title - updated\", \"subtext\":\"test subtext - updated\", \"maincontent\" : \"test maincontent - updated\"}"
	payload := []byte(testJson)
	request, _ := http.NewRequest("PUT", "/api/post/1", bytes.NewBuffer(payload))
	response = helper.ExecuteRequest(request, &a)

	var updatedPost map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &updatedPost)
	return updatedPost
}

func thenUpdatedPostShouldBeDifferentThanOriginal(t *testing.T, updatedPost map[string]interface{}, originalPost map[string]interface{}) {
	if updatedPost["id"] != originalPost["id"] {
		t.Errorf("Expected the id to remain the same (%v). Got %v", originalPost["id"], updatedPost["id"])
	}

	if updatedPost["title"] == originalPost["title"] {
		t.Errorf("Expected the title to change from '%v' to '%v'. Got '%v'", originalPost["title"], updatedPost["title"], updatedPost["title"])
	}

	if updatedPost["subtext"] == originalPost["subtext"] {
		t.Errorf("Expected the subtext to change from '%v' to '%v'. Got '%v'", originalPost["subtext"], updatedPost["subtext"], updatedPost["subtext"])
	}

	if updatedPost["maincontent"] == originalPost["maincontent"] {
		t.Errorf("Expected the price to change from '%v' to '%v'. Got '%v'", originalPost["maincontent"], updatedPost["maincontent"], updatedPost["maincontent"])
	}
}

func whenIMakeARequestToDeleteThatPost(){
	request, _ := http.NewRequest("DELETE", "/api/post/1", nil)
	response = helper.ExecuteRequest(request, &a)
}


func thenIGetNComments(t *testing.T, numberOfComments int) {
	var comments []structs.Comment
	bytes := response.Body.Bytes()
	json.Unmarshal(bytes, &comments)
	if len(comments) != numberOfComments {
		t.Errorf("Expected the %d comments but got %d", numberOfComments, len(comments))
	}
}



func whenIMakeARequestToGetCommentsOfSecondPost() {
	request, _ := http.NewRequest("GET", "/api/post/2/comments", nil)
	response = helper.ExecuteRequest(request, &a)
}

func whenIMakeARequestToGetCommentsOfFirstPost() {
	request, _ := http.NewRequest("GET", "/api/post/1/comments", nil)
	response = helper.ExecuteRequest(request, &a)
}

func givenThereAreTwoPostsInDatabaseWithComments() {
	givenAllTablesAreClear()
	helper.AddPosts(2, &a)
	helper.AddComment(1, &a)
	helper.AddComment(1, &a)
	helper.AddComment(1, &a)
	helper.AddComment(1, &a)
	helper.AddComment(2, &a)
	helper.AddComment(2, &a)
	helper.AddComment(2, &a)

}

func thenIGet400WithErrorMessage(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusBadRequest, response.Code)

	var actualResponseMap map[string]string
	json.Unmarshal(response.Body.Bytes(), &actualResponseMap)
	if actualResponseMap["error"] != "Post not found" {
		t.Errorf("Expected the 'error' key of the response to be set to 'Post not found'. Got '%s'", actualResponseMap["error"])
	}
}

func thenIGet200AndNonEmptyListOfComments(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected a non-empty array. Got %s.", body)
	}
}

func givenThereIsOnePostInDatabaseWithComments() {
	givenThereIsOnePostInDatabase()
	helper.AddComment(1, &a)
}

func whenIMakeARequestToGetCommentsOfNonExistingPost() {
	request, _ := http.NewRequest("GET", "/api/post/11/comments", nil)
	response = helper.ExecuteRequest(request, &a)
}

func thenIGet204AndEmptyListOfComments(t *testing.T) {
	helper.CheckResponseCode(t, http.StatusNoContent, response.Code)

	if body := response.Body.String(); body != "[]" {
		t.Errorf("Expected an empty array. Got %s.", body)
	}
}


func givenThereIsOnePostInDatabaseWithoutComments() {
	givenThereIsOnePostInDatabase()
}

func whenIMakeARequestToGetCommentsOfThatPost() {
	request, _ := http.NewRequest("GET", "/api/post/1/comments", nil)
	response = helper.ExecuteRequest(request, &a)
}