package main_test

import (
	"testing"
)

func TestEmptyTable(t *testing.T) {
	givenAllTablesAreClear()
	whenIMakeARequestToGetAllPosts()
	thenIGet204AndEmptyListOfPosts(t)
}

func TestGetNonExistentPost(t *testing.T) {
	givenAllTablesAreClear()
	whenIMakeARequestToGetAPostThatDoesNotExist()
	thenIGet204WithErrorMessage(t)
}

func TestCreatePost(t *testing.T) {
	givenAllTablesAreClear()
	whenIMakeAPOSTRequestToCreateANewPost()
	thenICreateANewPostAndAllFieldsAreCorrectlyAdded(t)
}

func TestGetProduct(t *testing.T) {
	givenThereIsOnePostInDatabase()
	whenIMakeARequestToGetThatPost()
	theIGet200AndThatPost(t)
}

func TestUpdateProduct(t *testing.T) {
	givenThereIsOnePostInDatabase()
	originalPost := whenIMakeARequestToGetCopyOfThatPost()
	updatedPost := andUpdateThatOnePost()
	thenUpdatedPostShouldBeDifferentThanOriginal(t, updatedPost, originalPost)
}

func TestDeletePost(t *testing.T) {
	givenThereIsOnePostInDatabase()

	//Checking if post is there
	whenIMakeARequestToGetThatPost()
	theIGet200AndThatPost(t)
	//Deleting that post
	whenIMakeARequestToDeleteThatPost()
	theIGet200AndThatPost(t)
	//Checking if post is deleted
	whenIMakeARequestToGetThatPost()
	thenIGet204WithErrorMessage(t)
}

