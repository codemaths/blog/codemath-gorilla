package helper

import (
	. "gitlab.com/vorashilf/restapi/main/app"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func CheckResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response %d. Got %d\n", expected, actual)
	}
}

func ClearAllTables(app *App) {
	clearCommentsTable(app)
	clearPostTable(app)
}

func ExecuteRequest(request *http.Request, app *App) *httptest.ResponseRecorder {
	responseRecorder := httptest.NewRecorder()
	app.Router.ServeHTTP(responseRecorder, request)

	return responseRecorder
}

func EnsureTablesExists(app *App) {
	if _, err := app.DB.Exec(postTableCreationQuery); err != nil {
		log.Print(err)
	}
}

func AddPosts(count int, app *App) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {
		app.DB.Exec("INSERT INTO blog.posts (title, subtext, main_content) VALUES($1, $2, $3) ",
			"test title "+strconv.Itoa(i), "test subtext "+strconv.Itoa(i), "test maincontent "+strconv.Itoa(i))
	}
}

func AddComment(postId int, app *App) {
	if postId < 1 {
		postId = 1
	}

	app.DB.Exec("INSERT INTO blog.comments(post_id, author, email, content) VALUES($1, $2, $3, $4) RETURNING id ",
		postId, "test author", "test@email.com", "test comment")

}

func clearCommentsTable(app *App) {
	_, _ = app.DB.Exec("DELETE FROM blog.comments")
	_, _ = app.DB.Exec("ALTER SEQUENCE blog.comments_id_seq RESTART WITH 1")
}

func clearPostTable(app *App) {
	_, _ = app.DB.Exec("DELETE FROM blog.posts")
	_, _ = app.DB.Exec("ALTER SEQUENCE blog.posts_id_seq RESTART WITH 1")
}

const postTableCreationQuery = `CREATE TABLE IF NOT EXISTS blog.posts
(
	id SERIAL,
	title TEXT NOT NULL,
	subtext TEXT,
	main_content TEXT NOT NULL,
	CONSTRAINT posts_pkey PRIMARY KEY (id)
)
`